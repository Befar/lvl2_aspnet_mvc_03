﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class EmployeeController : Controller
    {
        HR14Entities db = new HR14Entities();
        public ActionResult Index()
        {
            return View(db.Employees.ToList());
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var remove1Employee = db.Employees.Where(x => x.id == 1).FirstOrDefault();
                        db.Employees.Remove(remove1Employee);
                        db.Employees.Add(employee);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }

                }
            }
            return View(employee);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            return View(db.Employees.Where(x => x.id == id).FirstOrDefault());
        }


        public ActionResult Edit(int id)
        {
            return View(db.Employees.Where(x => x.id == id).FirstOrDefault());
        }
        public ActionResult Delete(int id)
        {
            return View(db.Employees.Where(x => x.id == id).FirstOrDefault());
        }
        [HttpPost]
        public ActionResult Edit(int id,Employee employee)
        {
 
                db.Entry(employee).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
        
        return RedirectToAction("Index");
        }
       
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {

            Employee employee =  db.Employees.Where(x => x.id == id).FirstOrDefault();
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}